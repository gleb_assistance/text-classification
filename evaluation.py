def accuracy(classifier, data):
    """Computes the accuracy of a classifier on reference data.

    Args:
        classifier: A classifier.
        data: Reference data.

    Returns:
        The accuracy of the classifier on the test data, a float.
    """
    ##################### STUDENT SOLUTION #########################
    tp, fp, fn, tn = get_conf_matrix(classifier, data)
    return (tp + tn) / (tp + fp + fn + tn)
    ################################################################



def f_1(classifier, data):
    """Computes the F_1-score of a classifier on reference data.

    Args:
        classifier: A classifier.
        data: Reference data.

    Returns:
        The F_1-score of the classifier on the test data, a float.
    """
    ##################### STUDENT SOLUTION #########################
    tp, fp, fn, tn = get_conf_matrix(classifier, data)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    return 2 * (precision * recall) / (precision + recall)
    ################################################################


def get_conf_matrix(classifier, data):
    tp = 0
    tn = 0
    fp = 0
    fn = 0
    for doc, y_true in data:
        y_pred = classifier.predict(doc)
        if y_true == 0:
            if y_true == y_pred:
                tp += 1
            else:
                fp += 1
        else:
            if y_pred == 0:
                tn += 1
            else:
                fn += 1
    return tp, fp, fn, tn
