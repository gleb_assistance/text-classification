import numpy as np
from collections import Counter


class NaiveBayes(object):

    ######################### STUDENT SOLUTION #########################
    # YOUR CODE HERE
    def __init__(self, logprior, loglikelihood_dist, v, labels_to_numbers, numbers_to_labels):
        """Initialises a new classifier."""
        self.logprior = logprior
        self.loglikelihood = loglikelihood_dist
        self.classes = len(logprior)
        self.vocabulary = v
        self.labels_to_numbers = labels_to_numbers
        self.numbers_to_labels = numbers_to_labels
    ####################################################################


    def predict(self, x):
        """Predicts the class for a document.

        Args:
            x: A document, represented as a list of words.

        Returns:
            The predicted class, represented as a string.
        """
        ################## STUDENT SOLUTION ########################
        answers = []
        for sample in x:
            best_class = [0] * self.classes

            for i in range(self.classes):
                best_class[i] = self.logprior[i]
                for word in sample[0]:
                    if word in self.vocabulary:
                        best_class[i] *= self.loglikelihood[i][word]

            answers.append(np.argmax(best_class))
        return answers
        ############################################################

    @classmethod
    def train(cls, data, k=1):
        """Train a new classifier on training data using maximum
        likelihood estimation and additive smoothing.

        Args:
            cls: The Python class representing the classifier.
            data: Training data.
            k: The smoothing constant.

        Returns:
            A trained classifier, an instance of `cls`.
        """
        ##################### STUDENT SOLUTION #####################
        labels_to_numbers, numbers_to_labels = NaiveBayes.label_encoding(data)

        n_classes = len(labels_to_numbers)
        n_docs = len(data)
        N_c = NaiveBayes.calc_class_docs(data, labels_to_numbers, n_classes)
        logprior = [np.log(N_c[i] / n_docs) for i in range(n_classes)]
        # notlogprior = [N_c[i] / n_docs for i in range(n_classes)]
        bigdoc = NaiveBayes.get_bigdoc(data, labels_to_numbers, n_classes)
        v = NaiveBayes.get_vocabulary(data)
        v_classes = NaiveBayes.get_classes_vocabulary(data, labels_to_numbers, n_classes)
        c = [Counter(bigdoc[i]) for i in range(len(bigdoc))]
        loglikelihood_dist = [dict() for _ in range(n_classes)]
        # likelihood_dist = [dict() for _ in range(n_classes)]
        for i in range(n_classes):
            for w in v:
                loglikelihood_dist[i][w] = np.log((c[i][w] + k) / (len(v) + len(v_classes[i]) + k))
                # likelihood_dist[i][w] = (c[i][w] + 1) / (len(v) + len(v_classes[i]) + 1)

        return cls(logprior, loglikelihood_dist, v, labels_to_numbers, numbers_to_labels)  # return it
        # return cls(notlogprior, likelihood_dist, v, labels_to_numbers, numbers_to_labels)
        ############################################################

    @staticmethod
    def get_vocabulary(data):
        vocabulary = set()
        for d in data:
            vocabulary.update(d[0])
        return vocabulary

    @staticmethod
    def get_classes_vocabulary(data, labels_to_numbers, classes):
        vocabularies = [set() for _ in range(classes)]
        for d in data:
            vocabularies[labels_to_numbers[d[1]]].update(d[0])
        return vocabularies

    @staticmethod
    def get_bigdoc(data, labels_to_numbers, classes):
        bigdoc = [[] for _ in range(classes)]
        for d in data:
            bigdoc[labels_to_numbers[d[1]]].extend(d[0])
        return bigdoc

    @staticmethod
    def calc_class_docs(data, labels_to_numbers, classes: int = 2) -> list:
        docs_distribution = [0] * classes
        for d in data:
            docs_distribution[labels_to_numbers[d[1]]] += 1
        return docs_distribution

    @staticmethod
    def label_encoding(data):
        s = set()
        labels_to_numbers = dict()
        numbers_to_labels = dict()
        for sample in data:
            s.add(sample[1])

        for i, d in enumerate(s):
            labels_to_numbers[d] = i
            numbers_to_labels[i] = d
        return labels_to_numbers, numbers_to_labels


def features1(data, k=1):
    """
    Your feature of choice for Naive Bayes classifier.

    Args:
        data: Training data.
        k: The smoothing constant.

    Returns:
        Parameters for Naive Bayes classifier, which can
        then be used to initialize `NaiveBayes()` class
    """
    ###################### STUDENT SOLUTION ##########################
    # YOUR CODE HERE
    return None
    ##################################################################


def features2(data, k=1):
    """
    Your feature of choice for Naive Bayes classifier.

    Args:
        data: Training data.
        k: The smoothing constant.

    Returns:
        Parameters for Naive Bayes classifier, which can
        then be used to initialize `NaiveBayes()` class
    """
    ###################### STUDENT SOLUTION ##########################
    # YOUR CODE HERE
    return None
    ##################################################################

